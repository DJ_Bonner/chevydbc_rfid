#!/usr/bin/python
"""
Alien RFID ALR-9900+ over network
24G LLC.
Process the tags and dont use their API
MUST HAVE MySQLdb
"""
__author__ = "DJ Bonner"
## PYTHON IMPORTS
import socket, select, string, sys, thread, MySQLdb, threading, signal, os
## CUSTOM IMPORTS
import settings, db_tasker
## SPECIFIC IMPORTS
from threading import Timer
from time import sleep
from datetime import datetime

db = None
f_thread = None

def _safe_exit(signal, frame):
	os.system("sudo killall -9 telnet.py")

def thread_poll(s):
	while True:
		s.send("t\n")		
		sleep(settings.TIMEOUT)

def start_file_watcher(form_tags):
	"""
	Allows callback for no db connection from timer
	"""	
	date = datetime.strftime(datetime.now(), '%Y-%m-%d_%H:%M:%S')
	try:
		fp = settings.path + '/files/' + date
		with open(fp, 'w') as f:
			for line in form_tags:
				f.write( '$$'.join( line ) + '\n' )	
		if not f_thread.isAlive():
			print "filewatcher crashed, restarting..."
			f_thread = threading.Thread( target = db_tasker.watch, args=())
			f_thread.start()
	except:
		return False
	return True

def process_tags(data, last_tags):
	"""
	Process the return given from telnet command t\n
	"""
	tags = data.split("Tag:")
	#tags = [noblank.strip() for noblank in tags] # get rid of trailing white spaces	
	if len(tags) > 1: # not an empty array
		last = len(tags[-1])
		last_tags[-1] += tags[0]
		last_tags += tags[1:]
		#print last
		if last < 114: # not a full message
			#print '\n'.join(tags),
			pass
		else: # last piece of message recieved					
			#print '\n'.join(tags)
			last_tags = filter(None, last_tags)
			num_tags = len(last_tags)
			if num_tags > 1:
				print '\n## %s TAGS ##\n' % str(len(last_tags))
			else:							
				print '\n## %s TAG ##\n' % str(len(last_tags))
			form_tags = []
			for tag in last_tags: # seperate data for each tag
				tmp = tag.split(',')
				tmp = [tag.strip() for tag in tmp]
				form_tags.append(tmp)
			### THIS IS THE POINT WHERE CONNECTIVITY IS HANDLED ###
			if not start_file_watcher(form_tags):
				#print "filewatcher returned False"
				pass
			#######################################################				
			last_tags = [''] 
	else: # empty array recieved
		#print "No data recieved"
		pass
	return last_tags

#main function
if __name__ == "__main__":
	try:
		print settings.pi_num
	except:
		print "You must run setup before running the main program"
		sys.exit()
	# catch any ctrl + c events
	signal.signal(signal.SIGINT, _safe_exit)
	 
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	#s.settimeout() DONT WANT THIS FOR NOW
	 
	# connect to remote host
	try :
		s.connect((settings.host, settings.port))
	except :
		print 'Unable to connect to alien rfid reader at %s:%s' % ( settings.host, settings.port )
		sys.exit()
	 
	print 'Connected to alien rfid reader at %s:%s' % ( settings.host, settings.port )
	s.send(settings.user + "\n")
	s.send(settings.password + "\n")
	f_thread = threading.Thread( target = db_tasker.watch, args=())
	f_thread.start()
	thread.start_new_thread(thread_poll, (s,))

	last_tags = ['']	 

	while 1:
		# Wait for data
		data = s.recv(16384)
		if not data : # game over
			print 'Connection closed'
			sys.exit()
		else : # deal with the data
			last_tags = process_tags(data, last_tags)
