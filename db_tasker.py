#!/usr/bin/python
"""
Alien RFID ALR-9900+ over network
24G LLC.
Process the tags and dont use their API
MUST HAVE MySQLdb

This is the handler attached to a file dump of data 
if there is no internet connection
"""
__author__ = "DJ Bonner"
# PYTHON IMPORTS
import MySQLdb, os, sys
# BUSTOM IMPORTS
import settings
# SPECIFIC IMPORTS
from time import sleep
from os import listdir
from os.path import isfile, join

db = None
stop = False

def stop_running():
	sys.exit()

def db_connect():
	global db
	try:
		db = MySQLdb.connect( settings.db_host, settings.db_user, settings.db_pass, settings.db_name )
	except:
		return False
	return True

def db_disconnect():
	db.close()

def construct_cmd(tags):
	cmd = "INSERT INTO `%s` (`timestamp`,`badge_id`,`scanner_id`, `antenna_id`) VALUES " % ( settings.db_table )
	const     = []
	scanner   = settings.pi_num
	antenna   = None
	last_seen = None
	for tag in tags:
		if len(tag) > 24:
			try:
				tmp = tag.split("$$")
				tag = tmp[0].strip()
				antenna   = [s for s in tmp if "Ant:" in s]
				antenna   = antenna[0].split(':')[1]
				last_seen = [s for s in tmp if "Last:" in s]
				last_seen = last_seen[0].split(':', 1)[1]
				last_seen = last_seen.split('.')[0]
				last_seen = last_seen.replace("/", "-")
				if tag == None or len(tag) == 0:
					tag = 'DATA MISSING'
					print "TAG MISSING"
				elif antenna == None or len(antenna) == 0:
					antenna = 'DATA MISSING'
					print "ANTENNA MISSING"
				elif last_seen == None or len(last_seen) == 0:
					last_seen = 'DATA MISSING'
					print "LAST_SEEN MISSING"
				const.append( "('%s','%s','%s','%s')" % (last_seen, tag, scanner, antenna))
			except:
				print "ERROR PARSING TAG, MOVING ON..."
	cmd = cmd + ','.join( const ) + ";"
	return cmd

def db_insert(cmd):
	print cmd
	try:
		cur = db.cursor()
		cur.execute(cmd)
		db.commit()
		return True
	except:
		return False

def process_tags(tags):
	try:
		cmd = construct_cmd(tags)
		if not db_insert(cmd):
			print "db_insert() failed, holding on to file to push later"
			return False
		else:
			pass
	except:
		print "#### Exception during db_insert(), DELETING CORRUPTED RECORD ####"
		return False
	return True

def watch():
	path = settings.path + '/files/'
	print "db_tasker.py started..."
	onlyfiles = [ f for f in listdir(path) if isfile(join(path,f)) ]
	if len(onlyfiles) > 0:
		print 'Files in queue:'
		print '\n'.join(onlyfiles)	
		print '###############'
	while True:
		if not db_connect():
			continue				
		else:	
			onlyfiles = [ f for f in listdir(path) if isfile(join(path,f)) ]
			if len(onlyfiles) > 0:
				print 'Processing files:'
				print '\n'.join(onlyfiles)
				print '#################'
				for fp in onlyfiles:				
					with open(path + fp, 'r') as f:
						tags = f.readlines()
					if len(tags) <= 1: # empty file
						os.remove(path + fp)
					else:
						tags = [tag.replace('\x00', '').strip() for tag in tags]
						tags = filter(None, tags)
						if not process_tags(tags):
							print "process_tags() returned false %s" % ( path + fp )
						else:
							print "#############################################"
							print "Successfully processed tags from file watcher : %s" % ( path + fp )
							os.remove(path + fp)
			db_disconnect()
	sys.exit()